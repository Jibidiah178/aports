# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-cryptography
pkgver=40.0.1
pkgrel=0
pkgdesc="Cryptographic recipes and primitives for Python"
url="https://cryptography.io/"
arch="all"
license="Apache-2.0 OR BSD-3-Clause"
depends="python3 py3-cffi"
makedepends="
	libffi-dev
	openssl-dev>3
	py3-gpep517
	py3-setuptools
	py3-setuptools-rust
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-hypothesis
	py3-iso8601
	py3-pretend
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-subtests
	py3-pytest-xdist
	py3-tz
	"
source="https://files.pythonhosted.org/packages/source/c/cryptography/cryptography-$pkgver.tar.gz
	https://files.pythonhosted.org/packages/source/c/cryptography_vectors/cryptography_vectors-$pkgver.tar.gz
	"
builddir="$srcdir/cryptography-$pkgver"

replaces="py-cryptography" # Backwards compatibility
provides="py-cryptography=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   39.0.1-r0:
#     - CVE-2023-23931
#   3.2.2-r0:
#     - CVE-2020-36242
#   3.2.1-r0:
#     - CVE-2020-25659

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2

	# prepare cryptography vectors for testing
	cd "$srcdir/cryptography_vectors-$pkgver"
	python3 setup.py build
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer dist/cryptography*.whl
	(
		cd "$srcdir"/cryptography_vectors-$pkgver
		"$builddir"/test-env/bin/python3 setup.py install
	)
	test-env/bin/python3 -m pytest -n $JOBS
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/cryptography-*.whl
}

sha512sums="
489f1d75fe3fab360d3ed5ca92efd2ce2ffde145810571a84414421d463a1d53af1bffd2c91cd0ca72237cf9072c59723219086af296fd15704ca557e76c5dee  cryptography-40.0.1.tar.gz
5908390f5d89fdea925753b8d2889ea7a26c3c27c5157dc7e1d43ed12dbc422812abe1cc8927167570dde4fb1df88cc5bc632fc2674463e74f6420aa086ae84f  cryptography_vectors-40.0.1.tar.gz
"
