# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=netdata-go-plugins
pkgver=0.51.4
pkgrel=1
pkgdesc="netdata go.d.plugin"
url="https://github.com/netdata/go.d.plugin"
arch="all !x86 !armv7 !armhf" # checks fail
license="GPL-3.0-or-later"
depends="netdata"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/netdata/go.d.plugin/tar.gz/refs/tags/v$pkgver"
builddir="$srcdir/go.d.plugin-$pkgver"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o go.d.plugin ./cmd/godplugin
}

check() {
	go test ./...
}

package() {
	 mkdir -p "$pkgdir/usr/lib/netdata/conf.d"
	 cp -r "$builddir/config/go.d.conf" "$builddir/config/go.d" "$pkgdir/usr/lib/netdata/conf.d/"

	 mkdir -p "$pkgdir/usr/libexec/netdata/plugins.d/"
	 install -D -m755 -t "$pkgdir/usr/libexec/netdata/plugins.d" "$builddir/go.d.plugin"
}

sha512sums="
38da708da5088fa2efcc9192112605f6fa5c4e8bb863220b3f0b7b747e02148b30d75665e5a9082b809ad61911027dbae1ec695a65907b78948477c50c5707c9  netdata-go-plugins-0.51.4.tar.gz
"
