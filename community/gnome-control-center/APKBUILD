# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Pablo Correa Gomez <ablocorrea@hotmail.com>
pkgname=gnome-control-center
pkgver=44.0
pkgrel=0
pkgdesc="GNOME control center"
url="https://gitlab.gnome.org/GNOME/gnome-control-center"
arch="all"
license="GPL-2.0-or-later"
depends="
	accountsservice
	dbus:org.freedesktop.Secrets
	colord
	cups-pk-helper
	openrc-settingsd
	"
makedepends="
	accountsservice-dev
	cairo-dev
	colord-dev
	colord-gtk-dev
	cups-dev
	docbook-xml
	docbook-xsl
	gcr-dev
	glib-dev
	gnome-bluetooth-dev
	gnome-desktop-dev
	gnome-online-accounts-dev
	gnome-settings-daemon-dev
	gsound-dev
	ibus-dev
	itstool
	krb5-dev
	libadwaita-dev
	libgtop-dev
	libgudev-dev
	libnma-dev
	libpwquality-dev
	libsecret-dev
	libwacom-dev
	libxml2-utils
	libxslt
	meson
	modemmanager-dev
	networkmanager-dev
	polkit-dev
	pulseaudio-dev
	samba-dev
	udisks2-dev
	upower-dev
	"
options="!check" # needs unpackaged py-dbusmock
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang $pkgname-doc $pkgname-bash-completion"
source="https://download.gnome.org/sources/gnome-control-center/${pkgver%.*}/gnome-control-center-$pkgver.tar.xz
	README.alpine
	manual-toggle-crash-fix.patch
	removable-media-fit-narrow.patch
	"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dibus=true \
		. output

	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	mkdir -p "$pkgdir"/usr/lib/pkgconfig
	mv "$pkgdir"/usr/share/pkgconfig/* "$pkgdir"/usr/lib/pkgconfig
	rmdir "$pkgdir"/usr/share/pkgconfig

	install -Dm0644 "$srcdir"/README.alpine -t "$pkgdir"/usr/share/doc/"$pkgname"
}

sha512sums="
c24a5ee6ea48a50d286799ddd520626d117e5708746e56698ba45d167db5bd555baef02609dc8a20f5d3b1c49322b9774db383e448ca7a09e5b9d1cd53176d01  gnome-control-center-44.0.tar.xz
350aa443149c71851ad4de0976c7fc5cb626ba0c57a8d41e6ef80da1c65ed84a4dfa2483ae92630a3b611c4bfa9360ded82b55e8cd0e3907294c025e4f6b1671  README.alpine
7471f70e6e653117a403dfc8768b3aa3c2be7d53ad0a0eccbc4c9d60fb53d6d4553939a44d77fc6433cfa40ddbbc9a9a77f12ae3c6e4ed5366d282806f796ee8  manual-toggle-crash-fix.patch
76f31f69c30484ace049551dff17b2bd933cdad43a18604a9e14ee3a26b250548dbba47f3cd73d5319d2bd3093649d5707677cc63f2c2e9fd91ff8a88c008a83  removable-media-fit-narrow.patch
"
