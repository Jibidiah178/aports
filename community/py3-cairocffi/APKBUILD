# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Stefan Wagner <stw@bit-strickerei.de>
# Maintainer: Newbyte <newbytee@protonmail.com>
pkgname=py3-cairocffi
_pkgname=cairocffi
pkgver=1.5.0
pkgrel=0
pkgdesc="Python CFFI-based binding to Cairo and GDK-PixBuf"
url="https://github.com/Kozea/cairocffi"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-cffi cairo gdk-pixbuf py3-xcffib"
makedepends="py3-gpep517 py3-setuptools py3-wheel py3-installer"
checkdepends="
	font-dejavu
	py3-pytest
	py3-pytest-cov
	py3-pytest-flake8
	py3-pytest-isort
"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # tests broken

replaces="py-cairocffi" # For backwards compatibiltiy
provides="py-cairocffi=$pkgver-r$pkgrel" # For backwards compatibility

prepare() {
	default_prepare
	sed /pytest-runner/d -i setup.cfg
}

build() {	
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/cairocffi*.whl
}

sha512sums="
32ee1d1217a0300512b60c11fc3c9a0456e4a74a9c5364840e51e52a1320a2e26a3d2ed761615ce5fee247bcd2c713c099d3419b731c37fa812040d5f6f3096b  cairocffi-1.5.0.tar.gz
"
