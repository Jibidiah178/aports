# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=epiphany
pkgver=44.1
pkgrel=1
pkgdesc="A simple, clean, beautiful view of the web"
url="https://wiki.gnome.org/Apps/Web"
arch="all"
license="GPL-3.0-or-later"
depends="
	dbus:org.freedesktop.Secrets
	gsettings-desktop-schemas
	gst-plugins-good
	"
makedepends="
	desktop-file-utils
	gcr4-dev
	gsettings-desktop-schemas-dev
	gst-plugins-base-dev
	gtk+3.0-dev
	icu-dev
	iso-codes-dev
	itstool
	json-glib-dev
	libadwaita-dev
	libarchive-dev
	libdazzle-dev
	libnotify-dev
	libportal-dev
	libsecret-dev
	libsoup-dev
	libxml2-dev
	meson
	nettle-dev
	sqlite-dev
	webkit2gtk-6.0-dev
	"
checkdepends="appstream-glib xvfb-run ibus"
subpackages="$pkgname-lang $pkgname-doc $pkgname-dbg"
source="https://download.gnome.org/sources/epiphany/${pkgver%.*}/epiphany-$pkgver.tar.xz"
options="!check" # broken

# secfixes:
#   42.2-r0:
#     - CVE-2022-29536
#   41.3-r0:
#     - CVE-2021-45085
#     - CVE-2021-45086
#     - CVE-2021-45087
#     - CVE-2021-45088

prepare() {
	default_prepare

	git init
}

build() {
	abuild-meson \
		-Db_lto=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	# https://gitlab.gnome.org/GNOME/epiphany/issues/829
	env PATH="$PATH:$builddir/output/src" xvfb-run meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
3f1736d3358d5898b04db7b57ebfe33d7becab4ebac4285ca96356e5c808bde7ee3db504da99afd34269a41741bc5b2e8d38ae5ffcbb513c585e85d9e2e46c19  epiphany-44.1.tar.xz
"
