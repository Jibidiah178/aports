# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-settings-daemon
pkgver=44.0
pkgrel=0
pkgdesc="GNOME settings daemon"
url="https://gitlab.gnome.org/GNOME/gnome-settings-daemon"
arch="all"
options="!check" # tests fail on builders
license="GPL-2.0-only AND LGPL-2.1-only"
depends="pulseaudio"
depends_dev="
	alsa-lib-dev
	colord-dev
	cups-dev
	geoclue-dev
	geocode-glib-dev>=3.26.4-r1
	lcms2-dev
	libcanberra-dev
	libgweather4-dev
	libnotify-dev
	libwacom-dev
	modemmanager-dev
	networkmanager-dev
	nss-dev
	pango-dev
	polkit-dev
	pulseaudio-dev
	upower-dev
	"
makedepends="
	$depends_dev
	gcr4-dev
	glib-dev
	gnome-desktop-dev
	gsettings-desktop-schemas-dev
	gtk+3.0-dev
	libxml2-utils
	meson
	"
checkdepends="
	gnome-session
	gnome-shell
	py3-dbusmock
	py3-gobject3
	umockdev-dev
	"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/gnome-settings-daemon/${pkgver%.*}/gnome-settings-daemon-$pkgver.tar.xz
	dont-use-logind-for-brightness-changing.patch
	"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}
sha512sums="
276d1c2e559e0297855cfc9fc7bd2ce4ae55e2b711222f1f1aae4802dc6499f51ea09bff8f44c5d7b9f0f5a32ab187c2ac4d8ee8e5f1754a186ac189b4e885da  gnome-settings-daemon-44.0.tar.xz
fd80b939a14bbc5dd502afda0bc6511f2d9c045018680e5ae7fbec32efadb564c5060ec91d374330f246d70571aad5979ce8c175175a29b5ccec3443c8286dc6  dont-use-logind-for-brightness-changing.patch
"
