# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libphonenumber
pkgver=8.13.9
pkgrel=0
pkgdesc="Library for parsing, formatting, and validating international phone numbers."
url="https://github.com/google/libphonenumber"
arch="all"
license="Apache-2.0"
depends_dev="
	abseil-cpp-dev
	boost-dev
	icu-dev
	protobuf-dev
	"
makedepends="$depends_dev
	cmake
	gtest-dev
	ninja
	"
checkdepends="icu-data-full"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/google/libphonenumber/archive/v$pkgver/libphonenumber-v$pkgver.tar.gz
	system-abseil.patch
	c++17.patch
	"

build() {
	cd cpp
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DREGENERATE_METADATA=OFF
	cmake --build build
}

check() {
	cd cpp
	./build/libphonenumber_test
}

package() {
	cd cpp
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b864f0ff25ed32813dfa7db5d92ada501566b4d6c366f6ee856dff82680631b88acf24def742015d112e20d4e8aa7c6312c04afb846d492d1f5bef93099775ec  libphonenumber-v8.13.9.tar.gz
03c1eb32208a868bcacf92eb42b1f3f009bcc898fa6446594a3e79baadbef5fb53bbe829adfb7d0c8ebebdd37898c6686ff837320d258cd47ef5de7a6bf341bc  system-abseil.patch
a8fb8cf592f3bd8c6b7c3bd3771fdb83e39dec2e546f6071074dfc27857ab0d39523086455b8626d8f1c90069eddf61568d9fc698923c20cbf398bcc4bb9a3f8  c++17.patch
"
