# Contributor: knuxify <knuxify@gmail.com>
# Maintainer: knuxify <knuxify@gmail.com>
pkgname=gtk-frdp
pkgver=0.2.0_git20230303
# only gnome-connections uses this, and they depend on this revision
_commit=3f991a22c025cad3016a7aa55988e51884964050
pkgrel=0
pkgdesc="RDP viewer widget for GTK"
url="https://gitlab.gnome.org/GNOME/gtk-frdp"
arch="all"
license="GPL-3.0-or-later AND LGPL-3.0-or-later"
depends="freerdp"
makedepends="meson gtk+3.0-dev vala glib-dev freerdp-dev gobject-introspection-dev"
depends_dev="gtk-frdp gtk+3.0-dev"
subpackages="$pkgname-dev $pkgname-viewer:_viewer"
source="https://gitlab.gnome.org/GNOME/gtk-frdp/-/archive/$_commit/gtk-frdp-$_commit.tar.gz"
builddir="$srcdir/gtk-frdp-$_commit"
options="!check" # no test suite

build() {
	abuild-meson \
		-Db_lto=true \
		-Dexamples=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_viewer() {
	pkgdesc="Demo app for gtk-frdp"
	amove usr/bin/gtk-frdp-viewer
}

sha512sums="
a15fe4d3b4c09eee6b9d0025c5e752ec7ed44f343461707f46b1da373ca981107f5c92d7aecb1636eff563f460d5d1f394bb59049415bcfb5ce551d09739d951  gtk-frdp-3f991a22c025cad3016a7aa55988e51884964050.tar.gz
"
