# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pytest
pkgver=7.3.0
pkgrel=0
pkgdesc="Python3 testing library"
url="https://docs.pytest.org/en/latest/"
arch="noarch"
license="MIT"
depends="
	py3-iniconfig
	py3-packaging
	py3-pluggy
	py3-py
	python3
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="bash py3-hypothesis py3-virtualenv py3-xmlschema"
source="https://files.pythonhosted.org/packages/source/p/pytest/pytest-$pkgver.tar.gz
	"
builddir="$srcdir/pytest-$pkgver"
options="!check" # causes bootstrapping issues because of checkdepends

replaces="pytest" # Backwards compatibility
provides="pytest=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare

	sed -e "/^\[metadata\]/a version = $pkgver" -i setup.cfg
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer dist/pytest*.whl

	test-env/bin/python3 -m pytest
}

package() {
	mkdir -p "$pkgdir"/usr/bin

	local name; for name in py.test pytest; do
		ln -s $name-3 "$pkgdir"/usr/bin/$name
	done

	python3 -m installer -d "$pkgdir" \
		dist/pytest*.whl
}

sha512sums="
9cb84d47529c53037774e89d30da38211ed372ae7ea5c8741d1d35b6299cb2a85be53ee29cd6d6cf9acc332acb3ed21cd95510712115cc670e7f13b06088bce8  pytest-7.3.0.tar.gz
"
