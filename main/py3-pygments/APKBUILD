# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Maintainer: Matt Smith <mcs@darkregion.net>
pkgname=py3-pygments
_pkgname=Pygments
pkgver=2.15.0
pkgrel=0
pkgdesc="Syntax highlighting package written in Python"
url="https://pygments.org/"
arch="noarch"
license="BSD-2-Clause"
depends="python3"
makedepends="py3-setuptools py3-gpep517 py3-wheel py3-installer"
checkdepends="py3-pytest py3-lxml py3-wcag-contrast-ratio"
subpackages="$pkgname-doc"
source="https://files.pythonhosted.org/packages/source/P/Pygments/Pygments-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # causes issues when bootstrapping (py3-pytest depends on this)

replaces="py-pygments" # Backwards compatibility
provides="py-pygments=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   2.7.4-r0:
#     - CVE-2021-20270

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/Pygments*.whl

	mkdir -p "$pkgdir"/usr/bin
	ln -s pygmentize "$pkgdir"/usr/bin/pygmentize-3
}

doc() {
	local destdir="$subpkgdir/usr/share/doc/$pkgname"

	cd "$builddir"

	install -m 644 -D doc/pygmentize.1 \
		"$subpkgdir"/usr/share/man/man1/pygmentize.1

	mkdir -p "$destdir"
	cp AUTHORS CHANGES LICENSE "$destdir"/

	# Note: The documentation in the doc directory needs to be generated
	# by py-sphinx
	cp -R ./doc/docs/* "$destdir"/

	default_doc
}

sha512sums="
5b9ca24ded1a16ea68d77d727ead635b3555c4c3dece735fb6c122aaef49b9f50bb8ef462cd60aac43eff88ed32415ec8e432e0ac5677bc02c14670f2aef97e4  Pygments-2.15.0.tar.gz
"
