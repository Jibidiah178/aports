# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeseal
pkgver=0.20.2
pkgrel=1
pkgdesc="One-way encrypted Secrets tool for Kubernetes"
url="https://sealed-secrets.netlify.app/"
arch="all"
license="Apache-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/bitnami-labs/sealed-secrets/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/sealed-secrets-$pkgver"

# secfixes:
#   0.19.0-r0:
#     - CVE-2022-32149

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-o bin/$pkgname \
		-ldflags "-X main.VERSION=$pkgver" \
		./cmd/kubeseal
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
9348ee052652799b833d291158769b9f8039f7270209443adc79f47335faca64ca876d1198de0dcbf2e8977a78471c25b56caf735a6965a41194ebcaa7931c51  kubeseal-0.20.2.tar.gz
"
