# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=soju
pkgver=0.6.0
pkgrel=2
pkgdesc="User-friendly IRC bouncer"
url="https://soju.im/"
license="AGPL-3.0"
arch="all"
pkgusers="$pkgname"
pkggroups="$pkgname"
install="$pkgname.pre-install"
depends="ca-certificates"
makedepends="go scdoc libcap-utils sqlite-dev"
subpackages="$pkgname-openrc $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~emersion/soju/archive/v$pkgver.tar.gz
	$pkgname.initd
	config.patch
	makefile.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make GOFLAGS="$GOFLAGS"
}

check() {
	go test ./...
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/soju

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname

	# add additional docs
	install -Dm644 -t "$pkgdir"/usr/share/doc/$pkgname doc/*.md
}

sha512sums="
eb62dfd77d3e91279b3a3f573ab7ba787038f1dda3498f615338d659f9efa5d7865a7647c1647da7d905e1250c2bd88f9e435e51330a10f9b67e012fda16daae  soju-0.6.0.tar.gz
5dd493da97b18355f927fb0a64de08a9f486122bef4d9be3780dd9ba49ff5142213920de52f11bf4ecca3504b3dc40777f2574fa7441b89717b5a42f0c19786b  soju.initd
2f09bdb2efd0cd2f5f138dac7d5a4d02cfee6c46fc103c30fa70a238f322617b53ffd4b6ce6d0a8410375dd7229a68dcd6318e2b9651c5b559fff70fbca7adb3  config.patch
b647576f50ab03a90d9ab9062d97ecf050fd6e7eeba77200e05e4577733886a4ca85c7cef68d476fc611bfca41f01eecca7d3f8ce8d03431872ccb78ea2f64db  makefile.patch
"
