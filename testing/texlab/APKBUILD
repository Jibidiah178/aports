# Maintainer: Dominika Liberda <ja@sdomi.pl>
# Contributor: Dominika Liberda <ja@sdomi.pl>
pkgname=texlab
pkgver=5.4.1
pkgrel=0
pkgdesc="An implementation of the Language Server Protocol for LaTeX"
url="https://github.com/latex-lsp/texlab"
# limited by rust/cargo
# armhf - fails to build
arch="x86_64 armv7 aarch64 x86 ppc64le"
license="GPL-3.0-or-later"
makedepends="cargo"
source="https://github.com/latex-lsp/texlab/archive/refs/tags/v$pkgver/texlab-v$pkgver.tar.gz"

# tests OOM on 32-bit
# x86_64/ppc64le tests broken with some things in /tmp
case "$CARCH" in
	x86|x86_64|ppc64le|armv7) options="!check" ;;
esac

export CARGO_PROFILE_RELEASE_PANIC="unwind"
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/texlab -t "$pkgdir"/usr/bin/
}

sha512sums="
6ecb5e987c640a25f520740596e46db402a62605f52396262a29f19e73d4a8c58451e4810cc6ff5aff1e30458ebd604054cf58a0cd44459727a3aacf33406a00  texlab-v5.4.1.tar.gz
"
