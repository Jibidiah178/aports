# Contributor: ungleich <foss@ungleich.ch>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: ungleich <foss@ungleich.ch>
pkgname=cri-o
pkgver=1.26.3
pkgrel=1
pkgdesc="OCI-based implementation of Kubernetes Container Runtime Interface"
url="https://github.com/cri-o/cri-o/"
arch="all"
license="Apache-2.0"
# Most tests will fail if not ran as root
# since it tries to create network interfaces
options="net chmod-clean !check"
depends="
	cni-plugins
	conntrack-tools
	containers-common
	iproute2
	iptables
	oci-runtime
	"
makedepends="
	bash
	btrfs-progs-dev
	eudev-dev
	glib-dev
	go
	go-md2man
	gpgme-dev
	libseccomp-dev
	libselinux-dev
	lvm2-dev
	ostree-dev
	tzdata
	"
checkdepends="bats cri-tools jq parallel sudo conmon"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	$pkgname-openrc
	"
source="
	https://github.com/cri-o/cri-o/archive/v$pkgver/cri-o-$pkgver.tar.gz
	crio.conf
	crio.initd
	crio.logrotated
	cni-plugins-path.patch
	makefile-fix-install.patch
	fix-test.patch
	remove-systemd-files.patch
	"

# secfixes:
#   1.23.2-r0:
#     - CVE-2022-0811
#   1.24.1-r0:
#     - CVE-2022-1708
#   1.26.2-r0:
#     - CVE-2022-4318

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOBIN="$GOPATH/bin"

build() {
	# https://github.com/cri-o/cri-o/blob/master/install.md#build-tags
	make BUILDTAGS="seccomp selinux apparmor containers_image_openpgp containers_image_ostree_stub"
}

check() {
	make localintegration
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr CRICTL_CONFIG_DIR="/etc/crio" OCIUMOUNTINSTALLDIR="/etc/crio" install

	# We want it in etc so apk does not overwrite it
	mkdir -p  "$pkgdir"/usr/share/oci-umount/oci-umount.d/
	ln -sf /etc/crio/crio-umount.conf "$pkgdir"/usr/share/oci-umount/oci-umount.d/crio-umount.conf

	# The CNI plugins are recommended to be installed as examples
	install -Dm644 contrib/cni/*.conflist -t "$pkgdir"/usr/share/doc/cri-o/examples/cni/

	install -Dm755 "$srcdir"/crio.initd "$pkgdir"/etc/init.d/crio
	install -Dm644 "$srcdir"/crio.conf "$pkgdir"/etc/crio/crio.conf
	install -Dm644 "$srcdir"/crio.logrotated "$pkgdir"/etc/logrotate.d/crio
}

sha512sums="
fb069579d08dd3cc1459578424b8e948c32be7165179c65c0bb86dbeda4d9bfc0897729a6522c5b1e0a4d0896c1984577b05590a23d0aa1b938e745efbbbce8f  cri-o-1.26.3.tar.gz
e026f056ed92489413e16ed7955a9dcd7d1f4df1cc28e3ea785771b44d43811fea4f5b953cc46bc0c4aeac8ad07115bfff304d7516ebd24f2e58fe782ff812c8  crio.conf
29561e95398975748236217bbd9df64997f6e3de6c0555d007306bd0535895a648368385a13079eb7d52c06249a91980523a73b6563e86d0575d9cd9c3fa4ee9  crio.initd
1115228546a696eeebeb6d4b3e5c3152af0c99a2559097fc5829d8b416d979c457b4b1789e0120054babf57f585d3f63cbe49949d40417ae7aab613184bf4516  crio.logrotated
0a567dfa431ab1e53f2a351689be8d588a60cc5fcdbda403ec4f8b6ab9b1c18ad425f6c47f9a5ab1491e3a61a269dc4efa6a59e91e7521fa2b6bb165074aa8e0  cni-plugins-path.patch
f9577aa7b1c90c6809010e9e406e65092251b6e82f6a0adbc3633290aa35f2a21895e1a8b6ba4b6375dcad3e02629b49a34ab16387e1c36eeb32c8f4dac74706  makefile-fix-install.patch
1c1bfa5feeb0c5ddc92271a5ef80edc38d56afa1574ffc124605d5bb227a407b55dd5268df6cebc6720768ac31245e08b7950e5ab2b7f14ba934c94f1e325f86  fix-test.patch
78c150f87027de489289596371dce0465159ced0758776b445deb58990e099de9c654406183c9da3cc909878b24d28db62121b7056cd180a6f2820e79e165cc6  remove-systemd-files.patch
"
